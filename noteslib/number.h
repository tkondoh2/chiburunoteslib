﻿#ifndef CHIBURU_NOTESLIB_NUMBER_H
#define CHIBURU_NOTESLIB_NUMBER_H

#include <noteslib/range.h>

namespace chiburu {
namespace noteslib {

class CHIBURUNOTESLIBSHARED_EXPORT Number
    : public Value
{
public:
  Number();

  virtual ~Number();

  Number(const Number &other);

  Number &operator=(const Number &other);

  virtual WORD type() const;

  virtual const char* typeName() const;

private:
  NUMBER value_;
};

class CHIBURUNOTESLIBSHARED_EXPORT NumberRange
    : public Range<Number>
{
public:
  NumberRange()
    : Range<Number>()
  {
  }

  virtual ~NumberRange()
  {
  }

  NumberRange(const NumberRange &other)
    : Range<Number>(other)
  {
  }

  NumberRange &operator=(const NumberRange &other)
  {
    if (this != &other)
    {
      Range<Number>::operator=(other);
    }
    return *this;
  }

  virtual WORD type() const
  {
    return TYPE_NUMBER_RANGE;
  }

  virtual const char* typeName() const
  {
    return "NumberRange";
  }
};

inline Number::Number()
  : Value()
  , value_(0.0)
{
}

inline Number::~Number()
{
}

inline Number::Number(const Number &other)
  : Value(other)
  , value_(other.value_)
{
}

inline Number &Number::operator=(const Number &other)
{
  if (this != &other)
  {
    Value::operator=(other);
    value_ = other.value_;
  }
  return *this;
}

} // namespace noteslib
} // namespace chiburu

Q_DECLARE_METATYPE(chiburu::noteslib::Number)
Q_DECLARE_METATYPE(chiburu::noteslib::NumberRange)

#endif // CHIBURU_NOTESLIB_NUMBER_H
