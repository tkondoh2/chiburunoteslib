#-------------------------------------------------
#
# Project created by QtCreator 2018-09-04T16:29:05
#
#-------------------------------------------------

# QtモジュールでGUIは使用しない
QT       -= gui

# ターゲット名
TARGET = ChiburuNotesLib

# テンプレートはライブラリ
TEMPLATE = lib

# ライブラリのエキスポート側を宣言
DEFINES += CHIBURUNOTESLIB_LIBRARY

# 次のように定義すると、廃止予定とマークされたQtの機能を使用すると警告が表示されます
# （正確な警告はコンパイラによって異なります）。
# コードを移植する方法を知るために、廃止予定のAPIのドキュメントを参照してください。
DEFINES += QT_DEPRECATED_WARNINGS

# 廃止予定のAPIを使用すると、コードのコンパイルに失敗することもあります。
# これを行うには、次の行のコメントを外します。
# Qtの特定のバージョンまで廃止予定のAPIを無効にすることもできます。
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # Qt 6.0.0より前に廃止されたすべてのAPIを無効にする

# ソースコード
SOURCES += \
    noteslib/string.cpp \
    noteslib/dir.cpp \
    noteslib/nationallanguage.cpp \
    noteslib/main.cpp \
    noteslib/exception.cpp \
    noteslib/number.cpp \
    noteslib/timedate.cpp \
    noteslib/variant.cpp \
    noteslib/namedsummarybuffer.cpp \
    noteslib/database.cpp

# ヘッダーコード
HEADERS += \
    noteslib/string.h \
    noteslib/dir.h \
    noteslib/nationallanguage.h \
    noteslib/main.h \
    noteslib/value.h \
    noteslib/noteslib_global.h \
    noteslib/exception.h \
    noteslib/number.h \
    noteslib/timedate.h \
    noteslib/range.h \
    noteslib/variant.h \
    noteslib/namedsummarybuffer.h \
    noteslib/database.h

# UNIX系の定義
unix {
    target.path = /usr/lib
    INSTALLS += target
}

#
# Notes API用マクロ定義
#
win32 {
    DEFINES += W W32 NT
    contains(QMAKE_TARGET.arch, x86_64) {
        DEFINES += W64 ND64 _AMD64_
    }
}
else:macx {
    # MACのDWORDをunsigned int(4バイト)にする
    # DEFINES += MAC
    DEFINES += MAC LONGIS64BIT
}
else:unix {
    DEFINES += UNIX LINUX W32
    QMAKE_CXXFLAGS += -std=c++0x
    target.path = /usr/lib
    INSTALLS += target
}

LIBS += -lnotes
