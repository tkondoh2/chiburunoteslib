﻿#ifndef CHIBURU_NOTESLIB_DIR_H
#define CHIBURU_NOTESLIB_DIR_H

#include <noteslib/string.h>
#include <QMetaType>
#include <rxcpp/rx.hpp>

namespace Rx {
using namespace rxcpp;
}

#if defined(NT)
#pragma pack(push, 1)
#endif

#include <nsfdata.h>
#include <nsfsearc.h>

#if defined(NT)
#pragma pack(pop)
#endif

namespace chiburu {
namespace noteslib {

class SearchParams;

class CHIBURUNOTESLIBSHARED_EXPORT Dir
{
public:
#if defined(NT)
  static const char delimiter = '\\';
#else
  static const char delimiter = '/';
#endif

  Dir();

  virtual ~Dir();

  Dir(const Dir &other);

  const Dir &operator=(const Dir &other);

  Dir(const String &path
      , const String &server = String()
      , const String &port = String()
      );

  const String &path() const { return path_; }
  const String &server() const { return server_; }
  const String &port() const { return port_; }

  String fileName() const;

  String netPath() const;

  void open() const;

  Rx::observable<bool> s_open() const;

  void close() const;

  bool isDirectory() const;

  QPair<String,String> pathNames() const;

  Rx::observable<QPair<String,String>> s_pathNames() const;

  virtual Rx::observable<SearchParams> s_getChildren(
      WORD noteClassMask
      , WORD searchFlags = SEARCH_SUMMARY // SEARCH_FILETYPE is added automatic
      ) const;

  static Rx::observable<String> s_getServerList(const String &port = String());

protected:
  Rx::observable<SearchParams> getChildrenStream(
      WORD noteClassMask
      , WORD searchFlags
      , FORMULAHANDLE hFormula = NULLHANDLE
      , const char *pViewTitle = nullptr
      , TIMEDATE *pSince = nullptr
      , TIMEDATE *retUntil = nullptr
      ) const;

private:
  String path_;
  String server_;
  String port_;
  mutable DBHANDLE handle_;
};

class CHIBURUNOTESLIBSHARED_EXPORT SearchParams
{
public:
  SearchParams();
  SearchParams(
      SEARCH_MATCH* pMatch
      , ITEM_TABLE* pTable
      );

  SEARCH_MATCH *pSearchMatch() const;
  ITEM_TABLE *pItemTable() const;

private:
  SEARCH_MATCH *pSearchMatch_;
  ITEM_TABLE *pItemTable_;
};

inline Dir::Dir()
  : path_()
  , server_()
  , port_()
  , handle_(NULLHANDLE)
{
}

inline Dir::~Dir()
{
  close();
}

inline Dir::Dir(const Dir &other)
  : path_(other.path_)
  , server_(other.server_)
  , port_(other.port_)
  , handle_(NULLHANDLE)
{
}

inline const Dir &Dir::operator=(const Dir &other)
{
  if (this != &other)
  {
    path_ = other.path_;
    server_ = other.server_;
    port_ = other.port_;
    handle_ = NULLHANDLE;
  }
  return *this;
}

inline Dir::Dir(const String &path
         , const String &server
         , const String &port
         )
  : path_(path)
  , server_(server)
  , port_(port)
  , handle_(NULLHANDLE)
{
}

inline SearchParams::SearchParams()
  : pSearchMatch_(nullptr)
  , pItemTable_(nullptr)
{
}

inline SearchParams::SearchParams(
    SEARCH_MATCH* pMatch
    , ITEM_TABLE* pTable
    )
  : pSearchMatch_(pMatch)
  , pItemTable_(pTable)
{
}

inline SEARCH_MATCH *SearchParams::pSearchMatch() const
{
  return pSearchMatch_;
}

inline ITEM_TABLE *SearchParams::pItemTable() const
{
  return pItemTable_;
}

} // namespace noteslib
} // namespace chiburu

#endif // CHIBURU_NOTESLIB_DIR_H
