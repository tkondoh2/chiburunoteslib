﻿#ifndef CHIBURU_NOTESLIB_VARIANT_H
#define CHIBURU_NOTESLIB_VARIANT_H

#include <noteslib/string.h>
#include <noteslib/number.h>
#include <noteslib/timedate.h>

namespace chiburu {
namespace noteslib {

class CHIBURUNOTESLIBSHARED_EXPORT Variant
{
public:
  Variant();

  virtual ~Variant();

  Variant(const Variant &other);

  Variant &operator=(const Variant &other);

  WORD type() const;

  Variant(const char *const pValue, int size);

  const char* value() const;

  int valueSize() const;

  String toString() const;

  Number toNumber() const;

  TimeDate toTimeDate() const;

  StringList toStringList() const;

  NumberRange toNumberRange() const;

  TimeDateRange toTimeDateRange() const;

  static Variant fromString(const String &value);

  static Variant fromNumber(const Number &value);

  static Variant fromTimeDate(const TimeDate &value);

  static Variant fromStringList(const String &value);

  static Variant fromNumberRange(const NumberRange &value);

  static Variant fromTimeDateRange(const TimeDateRange &value);

private:
  QByteArray bytes_;
};

inline Variant::Variant()
  : bytes_()
{
}

inline Variant::~Variant()
{
}

inline Variant::Variant(const Variant &other)
  : bytes_(other.bytes_)
{
}

inline Variant &Variant::operator=(const Variant &other)
{
  if (this != &other)
  {
    bytes_ = other.bytes_;
  }
  return *this;
}

inline WORD Variant::type() const
{
  if (bytes_.isEmpty() || bytes_.size() < static_cast<int>(sizeof(WORD)))
    return TYPE_UNAVAILABLE;

  return *reinterpret_cast<WORD*>(const_cast<char*>(bytes_.constData()));
}

inline Variant::Variant(const char *const pValue, int size)
  : bytes_(pValue, size)
{
}

inline const char* Variant::value() const
{
  return bytes_.constData() + sizeof(WORD);
}

inline int Variant::valueSize() const
{
  return bytes_.size() - static_cast<int>(sizeof(WORD));
}


} // namespace noteslib
} // namespace chiburu

#endif // VARIANT_H
