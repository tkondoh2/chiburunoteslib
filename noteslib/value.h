﻿#ifndef CHIBURU_NOTESLIB_VALUE_H
#define CHIBURU_NOTESLIB_VALUE_H

#include <noteslib/noteslib_global.h>
#include <QMetaType>

#if defined(NT)
#pragma pack(push, 1)
#endif

#include <nsfdata.h>

#if defined(NT)
#pragma pack(pop)
#endif

namespace chiburu {
namespace noteslib {

class CHIBURUNOTESLIBSHARED_EXPORT Value
{
public:
  Value();

  virtual ~Value();

  Value(const Value &other);

  Value &operator=(const Value &other);

  virtual WORD type() const = 0;

  virtual const char* typeName() const = 0;
};

inline Value::Value()
{
}

inline Value::~Value()
{
}

inline Value::Value(const Value &)
{
}

inline Value &Value::operator=(const Value &)
{
  return *this;
}

} // namespace noteslib
} // namespace chiburu

#endif // VALUE_H
