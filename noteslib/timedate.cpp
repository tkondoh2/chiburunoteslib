﻿#include "timedate.h"

#if defined(NT)
#pragma pack(push, 1)
#endif

#include <ostime.h>
//#include <intl.h>
#include <misc.h>

#if defined(NT)
#pragma pack(pop)
#endif

namespace chiburu {
namespace noteslib {

TimeDate::TimeDate()
  : Value()
  , value_(minimum())
{
}

WORD TimeDate::type() const
{
  return TYPE_TIME;
}

const char* TimeDate::typeName() const
{
  return "TimeDate";
}

TIMEDATE TimeDate::minimum()
{
  return constant<TIMEDATE_MINIMUM>();
}

TIMEDATE TimeDate::maximum()
{
  return constant<TIMEDATE_MAXIMUM>();
}

TIMEDATE TimeDate::wildcard()
{
  return constant<TIMEDATE_WILDCARD>();
}

} // namespace noteslib
} // namespace chiburu
