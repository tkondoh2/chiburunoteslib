﻿#ifndef CHIBURU_NOTESLIB_NATIONALLANGUAGE_H
#define CHIBURU_NOTESLIB_NATIONALLANGUAGE_H

#include <noteslib/noteslib_global.h>

#if defined(NT)
#pragma pack(push, 1)
#endif

#include <nls.h>
#include <osmisc.h>

#if defined(NT)
#pragma pack(pop)
#endif

namespace chiburu {
namespace noteslib {

class CHIBURUNOTESLIBSHARED_EXPORT NationalLanguage
{
public:
  NationalLanguage();

  NationalLanguage(WORD charset);

  virtual ~NationalLanguage();

  NLS_PINFO pInfo() const;

  void loadLmbcs();

  void load(WORD charset);

  void unload();

  QByteArray translate(
      WORD sourceCharset
      , const char* source
      , WORD sourceSize
      , WORD targetCharset
      , WORD bufferSize
      , WORD options
      ) const;

private:
  NLS_PINFO pInfo_;
  bool hasLoaded_;
};

class CHIBURUNOTESLIBSHARED_EXPORT NLSException
    : public std::exception
{
public:
  NLSException(NLS_STATUS s = NLS_SUCCESS);

  NLS_STATUS status() const;

  virtual const char* what() const;

private:
  NLS_STATUS status_;
};

inline NationalLanguage::NationalLanguage()
  : pInfo_(nullptr)
  , hasLoaded_(false)
{
}

inline NationalLanguage::NationalLanguage(WORD charset)
  : pInfo_(nullptr)
  , hasLoaded_(false)
{
  load(charset);
}

inline NationalLanguage::~NationalLanguage()
{
  unload();
}

inline NLS_PINFO NationalLanguage::pInfo() const
{
  return pInfo_;
}

inline NLS_STATUS NLSException::status() const
{
  return status_;
}

} // namespace noteslib
} // namespace chiburu

#endif // CHIBURU_NOTESLIB_NATIONALLANGUAGE_H
