﻿#ifndef CHIBURU_NOTESLIB_NAMEDSUMMARYBUFFER_H
#define CHIBURU_NOTESLIB_NAMEDSUMMARYBUFFER_H

#include <noteslib/variant.h>
#include <QMap>
#include <QList>

namespace chiburu {
namespace noteslib {

class CHIBURUNOTESLIBSHARED_EXPORT NamedSummaryBuffer
{
public:
  NamedSummaryBuffer();

  virtual ~NamedSummaryBuffer();

  NamedSummaryBuffer(const NamedSummaryBuffer &other);

  NamedSummaryBuffer &operator=(const NamedSummaryBuffer &other);

  NamedSummaryBuffer(const ITEM_TABLE *const pItemTable);

  Variant value(const String &name) const;

  StringList keys() const;

private:
  QMap<String, Variant> data_;
};

inline NamedSummaryBuffer::NamedSummaryBuffer()
  : data_()
{
}

inline NamedSummaryBuffer::~NamedSummaryBuffer()
{
}

inline NamedSummaryBuffer::NamedSummaryBuffer(const NamedSummaryBuffer &other)
  : data_(other.data_)
{
}

inline NamedSummaryBuffer &NamedSummaryBuffer::operator=(const NamedSummaryBuffer &other)
{
  if (this != &other)
  {
    data_ = other.data_;
  }
  return *this;
}

inline Variant NamedSummaryBuffer::value(const String &name) const
{
  return data_.value(name);
}

inline StringList NamedSummaryBuffer::keys() const
{
  return data_.keys();
}

} // namespace noteslib
} // namespace chiburu

#endif // CHIBURU_NOTESLIB_NAMEDSUMMARYBUFFER_H
