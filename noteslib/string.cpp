﻿#include "string.h"
#include "nationallanguage.h"
#include "exception.h"
#include <QString>
#include <QList>

#if defined(NT)
#pragma pack(push, 1)
#endif

#include <textlist.h>

#if defined(NT)
#pragma pack(pop)
#endif

namespace chiburu {
namespace noteslib {

QList<String> String::split(char delimiter) const
{
  QList<QByteArray> array = value_.split(delimiter);
  QList<String> list;
  foreach (QByteArray item, array) {
    list.append(String(item));
  }
  return list;
}

QString String::toQString() const
{
  try {
    NationalLanguage langUnicode(NLS_CS_UNICODE);
    QByteArray bytes = langUnicode.translate(
          NLS_SOURCEISLMBCS
          , value_.constData()
          , static_cast<WORD>(value_.size())
          , NLS_TARGETISUNICODE
          , UTF16_MAX_SIZE
          , NLS_NONULLTERMINATE
          );
    return QString::fromUtf16(
          reinterpret_cast<ushort*>(const_cast<char*>(bytes.constData()))
          , bytes.size() / static_cast<int>(UTF16_SIZE)
          );
  }
  catch (...)
  {
    throw std::current_exception();
  }
}

String String::fromQString(const QString &qstr)
{
  try {
    WORD maxCharSize = qstr.size() < static_cast<int>(UTF16_MAX_CHARS)
        ? static_cast<WORD>(qstr.size())
        : UTF16_MAX_CHARS;
    NationalLanguage langLmbcs;
    langLmbcs.loadLmbcs();
    QByteArray bytes = langLmbcs.translate(
          NLS_SOURCEISUNICODE
          , reinterpret_cast<const char*>(qstr.utf16())
          , maxCharSize * UTF16_SIZE
          , NLS_TARGETISLMBCS
          , LMBCS_MAX_SIZE
          , NLS_NONULLTERMINATE
          );
    return String(bytes.constData(), bytes.size());
  }
  catch (...)
  {
    throw std::current_exception();
  }
}

WORD String::type() const
{
  return TYPE_TEXT;
}

const char* String::typeName() const
{
  return "String";
}

WORD StringList::type() const
{
  return TYPE_TEXT_LIST;
}

const char* StringList::typeName() const
{
  return "StringList";
}

StringList::StringList(const LIST* const pList)
  : Value()
  , list_()
{
  WORD wSize = ListGetNumEntries(pList, FALSE);
  for (WORD i = 0; i < wSize; ++i)
  {
    char* pValue;
    WORD wValueLen;
    STATUS status = ListGetText(pList, 0, i, &pValue, &wValueLen);
    if (ERR(status) != NOERROR)
      throw Exception(status);

    String item(pValue, wValueLen);
    append(item);
  }
}

} // namespace noteslib
} // namespace chiburu
