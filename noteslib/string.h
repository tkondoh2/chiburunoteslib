﻿#ifndef CHIBURU_NOTESLIB_STRING_H
#define CHIBURU_NOTESLIB_STRING_H

#include <noteslib/value.h>
#include <QByteArray>
#include <QList>

namespace chiburu {
namespace noteslib {

class CHIBURUNOTESLIBSHARED_EXPORT String
    : public Value
{
public:
  /**
   * @brief LMBCS文字列の最大サイズ(バイト単位)
   */
  static const WORD LMBCS_MAX_SIZE = 0xfffe;

  /**
   * @brief Unicode文字のサイズ
   */
  static const std::size_t UTF16_SIZE = sizeof(ushort);

  /**
   * @brief Unicode文字列の最大文字数(文字単位)
   */
  static const WORD UTF16_MAX_CHARS = LMBCS_MAX_SIZE / UTF16_SIZE;

  /**
   * @brief Unicode文字列の最大サイズ(バイト単位)
   */
  static const WORD UTF16_MAX_SIZE = UTF16_MAX_CHARS * UTF16_SIZE;

  String();

  String(const char* data, int size = -1);

  virtual ~String();

  String(const String &other);

  String &operator=(const String &other);

  bool isEmpty() const;

  const char *constData() const;

  QList<String> split(char delimiter) const;

  QString toQString() const;

  static String fromQString(const QString &qstr);

  virtual WORD type() const;

  virtual const char* typeName() const;

  friend bool operator ==(const String& lhs, const String& rhs);
  friend bool operator !=(const String& lhs, const String& rhs);

  friend bool operator <(const String& lhs, const String& rhs);
  friend bool operator >(const String& lhs, const String& rhs);
  friend bool operator <=(const String& lhs, const String& rhs);
  friend bool operator >=(const String& lhs, const String& rhs);

private:
  QByteArray value_;
};

class CHIBURUNOTESLIBSHARED_EXPORT StringList
    : public Value
{
public:
  StringList();

  virtual ~StringList();

  StringList(const StringList &other);

  StringList &operator=(const StringList &other);

  virtual WORD type() const;

  virtual const char* typeName() const;

  StringList(const LIST* const pList);

  StringList(const QList<String> list);

  const String &operator[](int index) const;

  void append(const String &item);

  QStringList toQStringList() const;

  friend StringList &operator<<(StringList &lhs, const String &rhs);

private:
  QList<String> list_;
};

inline String::String()
  : Value()
  , value_()
{
}

inline String::String(const char* data, int size)
  : Value()
  , value_(data, size)
{
}

inline String::~String()
{
}

inline String::String(const String &other)
  : Value(other)
  , value_(other.value_)
{
}

inline String &String::operator=(const String &other)
{
  if (this != &other)
  {
    Value::operator=(other);
    value_ = other.value_;
  }
  return *this;
}

inline bool String::isEmpty() const
{
  return value_.isEmpty();
}

inline const char *String::constData() const
{
  return value_.constData();
}

inline StringList::StringList()
  : Value()
  , list_()
{
}

inline StringList::~StringList()
{
}

inline StringList::StringList(const StringList &other)
  : Value(other)
  , list_(other.list_)
{
}

inline StringList &StringList::operator=(const StringList &other)
{
  if (this != &other)
  {
    Value::operator=(other);
    list_ = other.list_;
  }
  return *this;
}

inline StringList::StringList(const QList<String> list)
  : Value()
  , list_(list)
{
}

inline const String &StringList::operator[](int index) const
{
  return list_[index];
}

inline void StringList::append(const String &item)
{
  list_.append(item);
}

inline QStringList StringList::toQStringList() const
{
  QStringList list;
  for (auto i = list_.constBegin(); i != list_.constEnd(); ++i)
  {
    list.append((*i).toQString());
  }
  return list;
}

inline bool operator ==(const String& lhs, const String& rhs)
{
  return (lhs.value_ == rhs.value_);
}

inline bool operator !=(const String& lhs, const String& rhs)
{
  return (lhs.value_ != rhs.value_);
}

inline bool operator <(const String& lhs, const String& rhs)
{
  return (lhs.value_ < rhs.value_);
}

inline bool operator >(const String& lhs, const String& rhs)
{
  return (lhs.value_ > rhs.value_);
}

inline bool operator <=(const String& lhs, const String& rhs)
{
  return (lhs.value_ <= rhs.value_);
}

inline bool operator >=(const String& lhs, const String& rhs)
{
  return (lhs.value_ >= rhs.value_);
}

inline StringList &operator<<(StringList &lhs, const String &rhs)
{
  lhs.append(rhs);
  return lhs;
}

} // namespace noteslib
} // namespace chiburu

Q_DECLARE_METATYPE(chiburu::noteslib::String)
Q_DECLARE_METATYPE(chiburu::noteslib::StringList)

#endif // CHIBURU_NOTESLIB_STRING_H
