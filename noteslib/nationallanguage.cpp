﻿#include "nationallanguage.h"
#include <QByteArray>

namespace chiburu {
namespace noteslib {

void NationalLanguage::loadLmbcs()
{
  unload();
  pInfo_ = OSGetLMBCSCLS();
}

void NationalLanguage::load(WORD charset)
{
  unload();
  NLS_STATUS status = NLS_load_charset(charset, &pInfo_);
  switch (status)
  {
  case NLS_SUCCESS:
    hasLoaded_ = true;
    break;

  default:
    throw NLSException(status);
  }
}

void NationalLanguage::unload()
{
  if (hasLoaded_)
  {
    NLS_unload_charset(pInfo_);
    hasLoaded_ = false;
  }
}

QByteArray NationalLanguage::translate(
    WORD sourceCharset
    , const char* source
    , WORD sourceSize
    , WORD targetCharset
    , WORD bufferSize
    , WORD options
    ) const
{
  // 変換後のUNICODE文字列格納用バッファ
  CharArrayPtr buffer(new char[bufferSize]);
  WORD retByteSize = bufferSize;

  NLS_STATUS status = NLS_translate(
        reinterpret_cast<BYTE*>(const_cast<char*>(source))
        , sourceSize
        , reinterpret_cast<BYTE*>(buffer.data())
        , &retByteSize
        , options | sourceCharset | targetCharset
        , pInfo_
        );

  switch (status)
  {
  case NLS_SUCCESS:
    break;

  default:
    throw NLSException(status);
  }

  return QByteArray(buffer.data(), retByteSize);
}

NLSException::NLSException(NLS_STATUS s)
  : std::exception()
  , status_(s)
{
}

const char* NLSException::what() const
{
  switch (status_) {
  case NLS_SUCCESS:
    return "Successful.";
  case NLS_BADPARM:
    return "The string, the character, or the NLS_INFO structure specified in the function call was invalid.";
  case NLS_BUFFERTOOSMALL:
    return "<Buffer too small>";
  case NLS_CHARSSTRIPPED:
    return "Characters are stripped.";
  case NLS_ENDOFSTRING:
    return "The string was empty.";
  case NLS_FALLBACKUSED:
    return "<Fallback used>";
  case NLS_FILEINVALID:
    return "<File invalid>";
  case NLS_FILENOTFOUND:
    return "<File not found>";
  case NLS_FINDFAILED:
    return "The string was empty, or the substring was not found in the string.";
  case NLS_INVALIDCHARACTER:
    return "<Invalid character>";
  case NLS_INVALIDDATA:
    return "<Invalid data>";
  case NLS_INVALIDENTRY:
    return "<Invalid entry>";
  case NLS_INVALIDTABLE:
    return "One of the tables in the specified NLS_INFO structure is invalid.";
  case NLS_PROPNOTFOUND:
    return "The specified character is NOT a control character.";
  case NLS_STARTOFSTRING:
    return "<Start of string>";
  case NLS_STRINGSIZECHANGED:
    return "<String size changed>";
  case NLS_TABLEHEADERINVALID:
    return "<Table header invalid>";
  case NLS_TABLENOTFOUND:
    return "<Table not found>";
  default:
    return "<Unknown NLS Error>";
  }
}

} // namespace noteslib
} // namespace chiburu
