﻿#include "variant.h"

namespace chiburu {
namespace noteslib {

String Variant::toString() const
{
  switch (type())
  {
  case TYPE_TEXT:
    return String(value(), valueSize());

  case TYPE_TEXT_LIST:
    return toStringList()[0];
  }
  return String();
}

StringList Variant::toStringList() const
{
  switch (type())
  {
  case TYPE_TEXT_LIST:
    return StringList(reinterpret_cast<LIST*>(const_cast<char*>(value())));

  case TYPE_TEXT:
  {
    StringList list;
    list.append(toString());
    return list;
  }
  }
  return StringList();
}

} // namespace noteslib
} // namespace chiburu
