﻿#ifndef CHIBURU_NOTESLIB_GLOBAL_H
#define CHIBURU_NOTESLIB_GLOBAL_H

#include <QtCore/qglobal.h>
#include <QScopedPointer>

#if defined(NT)
#pragma pack(push, 1)
#endif

#include <global.h>

#if defined(NT)
#pragma pack(pop)
#endif

#ifdef NULLHANDLE
#undef NULLHANDLE
#endif
#define NULLHANDLE nullptr

#if defined(CHIBURUNOTESLIB_LIBRARY)
#  define CHIBURUNOTESLIBSHARED_EXPORT Q_DECL_EXPORT
#else
#  define CHIBURUNOTESLIBSHARED_EXPORT Q_DECL_IMPORT
#endif

namespace chiburu {
namespace noteslib {

/**
 * @brief 文字列配列用スコープドスマートポインタ
 */
using CharArrayPtr = QScopedPointer<char, QScopedPointerArrayDeleter<char>>;

} // namespace noteslib
} // namespace chiburu

#endif // CHIBURU_NOTESLIB_GLOBAL_H
