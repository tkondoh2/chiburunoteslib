﻿#include "database.h"

namespace Rx {
using namespace rxcpp::operators;
}

namespace chiburu {
namespace noteslib {

Rx::observable<SearchParams> Database::s_getChildren(
    WORD noteClassMask
    , WORD searchFlags
    ) const
{
  return s_open()
  .flat_map([this, noteClassMask, searchFlags](bool) {
    return getChildrenStream(noteClassMask, searchFlags);
  });
}

} // namespace noteslib
} // namespace chiburu
