﻿#ifndef CHIBURU_NOTESLIB_EXCEPTION_H
#define CHIBURU_NOTESLIB_EXCEPTION_H

#include <noteslib/noteslib_global.h>

namespace chiburu {
namespace noteslib {

class CHIBURUNOTESLIBSHARED_EXPORT Exception
    : public std::exception
{
public:
  Exception(STATUS s = NOERROR);

  STATUS status() const;

  virtual const char *what() const;

  STATUS error() const;

  bool isRemote() const;

private:
  STATUS status_;
  mutable char what_[256];
};

inline STATUS Exception::status() const
{
  return status_;
}

inline STATUS Exception::error() const
{
  return ERR(status_);
}

inline bool Exception::isRemote() const
{
  return ((STS_REMOTE & status_) != 0);
}

} // namespace noteslib
} // namespace chiburu

#endif // CHIBURU_NOTESLIB_EXCEPTION_H
