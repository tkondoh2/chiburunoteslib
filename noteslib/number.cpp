﻿#include "number.h"

namespace chiburu {
namespace noteslib {

WORD Number::type() const
{
  return TYPE_NUMBER;
}

const char* Number::typeName() const
{
  return "Number";
}

} // namespace noteslib
} // namespace chiburu
