﻿#ifndef CHIBURU_NOTESLIB_MAIN_H
#define CHIBURU_NOTESLIB_MAIN_H

#include <noteslib/noteslib_global.h>

namespace chiburu {
namespace noteslib {

class CHIBURUNOTESLIBSHARED_EXPORT Main
{
public:
  Main(int argc, char **argv);

  virtual ~Main();

  static STATUS start(int argc, char **argv);

  static void end();

private:
  STATUS result_;
};

} // namespace noteslib
} // namespace chiburu

#endif // CHIBURU_NOTESLIB_MAIN_H
