﻿#ifndef CHIBURU_NOTESLIB_DATABASE_H
#define CHIBURU_NOTESLIB_DATABASE_H

#include <noteslib/dir.h>

namespace chiburu {
namespace noteslib {

class CHIBURUNOTESLIBSHARED_EXPORT Database
    : public Dir
{
public:
  Database();

  virtual ~Database();

  Database(const Database &other);

  const Database &operator=(const Database &other);

  Database(const String &path
      , const String &server = String()
      , const String &port = String()
      );

  virtual Rx::observable<SearchParams> s_getChildren(
      WORD noteClassMask
      , WORD searchFlags = SEARCH_SUMMARY
      ) const;
};

inline Database::Database()
  : Dir()
{
}

inline Database::~Database()
{
}

inline Database::Database(const Database &other)
  : Dir(other)
{
}

inline const Database &Database::operator=(const Database &other)
{
  if (this != &other)
  {
    Dir::operator=(other);
  }
  return *this;
}

inline Database::Database(
    const String &path
    , const String &server
    , const String &port
    )
  : Dir(path, server, port)
{
}

} // namespace noteslib
} // namespace chiburu

#endif // CHIBURU_NOTESLIB_DATABASE_H
