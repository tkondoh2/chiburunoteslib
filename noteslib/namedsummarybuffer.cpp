﻿#include "namedsummarybuffer.h"

namespace chiburu {
namespace noteslib {

NamedSummaryBuffer::NamedSummaryBuffer(const ITEM_TABLE *const pItemTable)
  : data_()
{
  ITEM *pItem = reinterpret_cast<ITEM*>(
        const_cast<ITEM_TABLE*>(pItemTable + 1)
        );
  char *pName = reinterpret_cast<char*>(pItem + pItemTable->Items);
  for (USHORT i = 0; i < pItemTable->Items; ++i, ++pItem)
  {
    char* pValue = pName + pItem->NameLength;
    String name(pName, pItem->NameLength);
    Variant value(pValue, pItem->ValueLength);
    data_[name] = value;
    pName = pValue + pItem->ValueLength;
  }
}

} // namespace noteslib
} // namespace chiburu
