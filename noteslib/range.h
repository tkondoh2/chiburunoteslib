﻿#ifndef CHIBURU_NOTESLIB_RANGE_H
#define CHIBURU_NOTESLIB_RANGE_H

#include <noteslib/value.h>
#include <QPair>
#include <QList>

namespace chiburu {
namespace noteslib {

template <class T>
class Pair
{
public:
  Pair()
    : pair_()
  {
  }

  virtual ~Pair()
  {
  }

  Pair(const Pair &other)
    : pair_(other.pair_)
  {
  }

  Pair &operator=(const Pair &other)
  {
    if (this != &other)
    {
      pair_ = other.pair_;
    }
    return *this;
  }

private:
  QPair<T, T> pair_;
};

template <class T>
class Range
    : public Value
{
public:
  Range()
    : Value()
    , list_()
    , pairs_()
  {
  }

  virtual ~Range()
  {
  }

  Range(const Range &other)
    : Value(other)
    , list_(other.list_)
    , pairs_(other.pairs_)
  {
  }

  Range &operator=(const Range &other)
  {
    if (this != &other)
    {
      Value::operator=(other);
      list_ = other.list_;
      pairs_ = other.pairs_;
    }
    return *this;
  }

private:
  QList<T> list_;
  QList<Pair<T>> pairs_;
};

} // namespace noteslib
} // namespace chiburu

#endif // CHIBURU_NOTESLIB_RANGE_H
