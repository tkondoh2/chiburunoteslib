﻿#ifndef CHIBURU_NOTESLIB_TIMEDATE_H
#define CHIBURU_NOTESLIB_TIMEDATE_H

#include <noteslib/range.h>

namespace chiburu {
namespace noteslib {

class CHIBURUNOTESLIBSHARED_EXPORT TimeDate
    : public Value
{
public:
  TimeDate();

  virtual ~TimeDate();

  TimeDate(const TimeDate &other);

  TimeDate &operator=(const TimeDate &other);

  virtual WORD type() const;

  virtual const char* typeName() const;

  static TIMEDATE minimum();
  static TIMEDATE maximum();
  static TIMEDATE wildcard();

protected:
  template <WORD type>
  static TIMEDATE constant()
  {
    TIMEDATE v;
    TimeConstant(type, &v);
    return v;
  }

private:
  TIMEDATE value_;
};

class CHIBURUNOTESLIBSHARED_EXPORT TimeDateRange
    : public Range<TimeDate>
{
public:
  TimeDateRange()
    : Range<TimeDate>()
  {
  }

  virtual ~TimeDateRange()
  {
  }

  TimeDateRange(const TimeDateRange &other)
    : Range<TimeDate>(other)
  {
  }

  TimeDateRange &operator=(const TimeDateRange &other)
  {
    if (this != &other)
    {
      Range<TimeDate>::operator=(other);
    }
    return *this;
  }

  virtual WORD type() const
  {
    return TYPE_TIME_RANGE;
  }

  virtual const char* typeName() const
  {
    return "TimeDateRange";
  }
};

inline TimeDate::~TimeDate()
{
}

inline TimeDate::TimeDate(const TimeDate &other)
  : Value(other)
  , value_(other.value_)
{
}

inline TimeDate &TimeDate::operator=(const TimeDate &other)
{
  if (this != &other)
  {
    Value::operator=(other);
    value_ = other.value_;
  }
  return *this;
}

} // namespace noteslib
} // namespace chiburu

Q_DECLARE_METATYPE(chiburu::noteslib::TimeDate)
Q_DECLARE_METATYPE(chiburu::noteslib::TimeDateRange)

#endif // CHIBURU_NOTESLIB_TIMEDATE_H
