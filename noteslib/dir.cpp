﻿#include "dir.h"
#include "exception.h"
#include <QPair>

#if defined(NT)
#pragma pack(push, 1)
#endif

#include <nsfdb.h>
#include <osfile.h>
#include <ns.h>
#include <osmem.h>

#if defined(NT)
#pragma pack(pop)
#endif

#include <QtDebug>

namespace Rx {
using namespace rxcpp::operators;
}

namespace chiburu {
namespace noteslib {

String Dir::fileName() const
{
  if (path_.isEmpty())
    return String();

  QList<String> s = path_.split(delimiter);
  return s.last();
}

String Dir::netPath() const
{
  char retPath[MAXPATH];
  STATUS status = OSPathNetConstruct(
        port_.isEmpty() ? nullptr : port_.constData()
        , server_.constData()
        , path_.constData()
        , retPath
        );
  switch (ERR(status))
  {
  case NOERROR:
    return String(retPath);

  default:
    throw Exception(status);
  }
}

void Dir::open() const
{
  if (handle_ != NULLHANDLE)
    return;

  String path = netPath();
  STATUS status = NSFDbOpen(path.constData(), &handle_);
  switch (ERR(status))
  {
  case NOERROR:
    break;

  default:
    throw Exception(status);
  }
}

Rx::observable<bool> Dir::s_open() const
{
  return Rx::observable<>::create<bool>(
        [this](Rx::subscriber<bool> s) {
    try {
      open();
      s.on_next(true);
    }
    catch (...)
    {
      s.on_error(std::current_exception());
    }
    s.on_completed();
  }
  );
}

void Dir::close() const
{
  if (handle_ != NULLHANDLE)
  {
    STATUS status = NSFDbClose(handle_);
    switch (ERR(status))
    {
    case NOERROR:
      handle_ = NULLHANDLE;
      break;

    default:
      throw Exception(status);
    }
  }
}

bool Dir::isDirectory() const
{
  open();
  USHORT mode;
  STATUS status = NSFDbModeGet(handle_, &mode);
  switch (ERR(status))
  {
  case NOERROR:
    return (mode == DB_DIRECTORY);

  default:
    throw Exception(status);
  }
}

QPair<String,String> Dir::pathNames() const
{
  open();
  char pCanonical[MAXPATH] = "";
  char pExtended[MAXPATH] = "";
  STATUS status = NSFDbPathGet(handle_, pCanonical, pExtended);
  switch (ERR(status))
  {
  case NOERROR:
    return QPair<String,String>(String(pCanonical),String(pExtended));

  default:
    throw Exception(status);
  }
}

Rx::observable<QPair<String,String>> Dir::s_pathNames() const
{
  return Rx::observable<>::just(pathNames());
}

Rx::observable<SearchParams> Dir::s_getChildren(
    WORD noteClassMask
    , WORD searchFlags
    ) const
{
  searchFlags |= SEARCH_FILETYPE;
  return s_open()
  .flat_map([this, noteClassMask, searchFlags](bool) {
    return getChildrenStream(noteClassMask, searchFlags);
  });
}

STATUS LNPUBLIC GetChildren(
    void *ptr
    , SEARCH_MATCH *pMatch
    , ITEM_TABLE *pItemTable
    )
{
  auto pSubscriber = reinterpret_cast<Rx::subscriber<SearchParams>*>(ptr);
  pSubscriber->on_next(SearchParams(pMatch, pItemTable));
  return NOERROR;
}

Rx::observable<SearchParams> Dir::getChildrenStream(
    WORD noteClassMask
    , WORD searchFlags
    , FORMULAHANDLE hFormula
    , const char *pViewTitle
    , TIMEDATE *pSince
    , TIMEDATE *retUntil
    ) const
{
  return Rx::observable<>::create<SearchParams>(
  [this
  , noteClassMask
  , searchFlags
  , hFormula
  , pViewTitle
  , pSince
  , retUntil
  ]
  (Rx::subscriber<SearchParams> s) {
    STATUS status = NSFSearch(
        handle_ // hDB
        , hFormula // hFormula
        , const_cast<char*>(pViewTitle) // ViewTitle
        , searchFlags // SearchFlags
        , noteClassMask // NoteClassMask
        , pSince // TIMEDATE *Since
        , GetChildren
        , reinterpret_cast<void*>(&s)
        , retUntil
        );
    switch (ERR(status))
    {
    case NOERROR:
      s.on_completed();
      break;

    default:
      throw Exception(status);
    }
  });
}

Rx::observable<String> Dir::s_getServerList(const String &port)
{
  return Rx::observable<>::create<String>([port](Rx::subscriber<String> s) {
    DHANDLE hServerList = nullptr;
    STATUS status = NSGetServerList(
          port.isEmpty() ? nullptr : const_cast<char*>(port.constData())
          , &hServerList
          );
    switch (ERR(status))
    {
    case NOERROR:
      Q_ASSERT(hServerList);
      break;

    default:
      throw Exception(status);
    }
    WORD *pwServerNameLen = reinterpret_cast<WORD*>(OSLockObject(hServerList));
    WORD wServerCount = *pwServerNameLen;
    ++pwServerNameLen;
    char *pServerName = reinterpret_cast<char*>(pwServerNameLen + wServerCount);
    for (WORD i = 0; i < wServerCount; ++i)
    {
      String serverName(pServerName, *pwServerNameLen);
      s.on_next(serverName);
      pServerName += *pwServerNameLen;
      ++pwServerNameLen;
    }
    OSUnlockObject(hServerList);
    OSMemFree(hServerList);
    s.on_completed();
  });
}

} // namespace noteslib
} // namespace chiburu
