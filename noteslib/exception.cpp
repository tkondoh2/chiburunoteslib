﻿#include "exception.h"

#if defined(NT)
#pragma pack(push, 1)
#endif

#include <osmisc.h>

#if defined(NT)
#pragma pack(pop)
#endif

namespace chiburu {
namespace noteslib {

Exception::Exception(STATUS s)
  : std::exception()
  , status_(s)
  , what_("")
{
}

const char *Exception::what() const
{
  WORD len = OSLoadString(NULLHANDLE, error(), what_, sizeof(what_));
  what_[len] = '\0';
  return what_;
}

} // namespace noteslib
} // namespace chiburu
